<?php

/**
 * @file
 * This is the page callback file for Not Taxable users module.
 */

 /**
  * Page callback function that returns HTML output.
  */
function not_taxable_users_admin($time_created = NULL) {
  $output = not_taxable_users_page();
  return $output;
}

/**
 * Add form to add a user to the table.
 */
function not_taxable_users_search() {
  $form = array();
  $label = t('Custom date');

  $form['search_all'] = array(
    '#type' => 'textfield',
    '#title' => t('User name'),
    'value' => '',
    '#size' => 30,
    '#required' => TRUE,
  );

  $form['search_en'] = array(
    '#type' => 'select',
    '#title' => t('Sales permit'),
    '#options' => array('No' => t('No'), 'Yes' => t('Yes')),
    '#default_value' => 'No',
  );

  $form['search_date'] = array(
    '#type' => 'date_popup',
    '#date_timezone' => date_default_timezone(),
    '#date_year_range' => '-3:+3',
    '#title' => $label,
    '#default_value' => '',
  );

  $form['#attached'] = array(
    'css' => array(drupal_get_path('module', 'not_taxable_users') . '/not_taxable_users.admin.css'),
    'js' => array(drupal_get_path('module', 'not_taxable_users') . '/js/not_taxable_users.js'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Form for editing user information in the table.
 */
function not_taxable_users_change() {
  $form = array();
  $label = t('Custom date');

  $form['notes_all'] = array(
    '#type' => 'hidden',
    'value' => '',
    '#prefix' => '<span id="lisok">',
    '#suffix' => '</span><br>',
  );

  $form['notes_en'] = array(
    '#type' => 'select',
    '#title' => t('Sales permit'),
    '#options' => array('No' => t('No'), 'Yes' => t('Yes')),
    '#default_value' => 'No',
  );

  $form['not_date'] = array(
    '#type' => 'date_popup',
    '#date_timezone' => date_default_timezone(),
    '#date_year_range' => '-3:+3',
    '#title' => $label,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Form for deletion user and his information from the table.
 */
function not_taxable_users_delete() {
  $form = array();
  $form['notes_one'] = array(
    '#type' => 'hidden',
    'value' => '',
    '#prefix' => '<span id="strumok">',
    '#suffix' => '</span><br>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Yes'),
  );

  $form['some_text'] = array(
    '#markup' => '<input class="form-submit exit" value = "' . t('Cancel') . '"></input>',
  );

  return $form;
}

/**
 * Implements function to render a table.
 */
function not_taxable_users_page() {
  $items = array();
  $not_taxable_users_search_item = drupal_get_form('not_taxable_users_search');
  $not_taxable_users_change_item = drupal_get_form('not_taxable_users_change');
  $not_taxable_users_delete_item = drupal_get_form('not_taxable_users_delete');
  $items['search_form'] = array(
    '#type' => 'markup',
    '#markup' => drupal_render($not_taxable_users_search_item),
    '#prefix' => '<div><p class="saler">' . t('Read help module') . ' <a href="' . url('admin/help/not_taxable_users') . '">page</a> ' . t('to set up conditions for your stores taxes') . '</p><div><div class = "formhead"><h2>' . t('Add user') . '</h2></div>',
    '#suffix' => '<h2 class="mantab">' . t('Managing table') . '</h2>',
  );

  $items['build'] = array();
  // Builds table header.
  $header = array(
    array('data' => t('Name'), 'field' => 'name', 'sort' => 'desc'),
    array('data' => t('Sales permit'), 'field' => 'not_taxable_users_val'),
    array('data' => t('Expiration date'), 'field' => 'exp_date'),
    array('data' => t('Edit user'), 'field' => 'taxid'),
    array('data' => t('Delete user'), 'field' => 'taxid'),
  );
  $rows = array();
  // Query to database for rendering table content.
  $query = db_select('not_taxable_users', 'n');
  $query->join('users', 'u', 'n.uid = u.uid');
  $result = $query
    ->fields('n', array('taxid', 'uid', 'not_taxable_users_val', 'exp_date'))
    ->fields('u', array('uid', 'name'))
    ->execute();
  foreach ($result as $rowses) {
    $name = check_plain($rowses->name);
    $datix = format_date($rowses->exp_date);

    $rows[] = array(
      array('data' => theme('username', array('account' => user_load_by_name($name)))),
      array('data' => $rowses->not_taxable_users_val, 'align' => 'center'),
      array('data' => $datix, 'align' => 'center'),
      // Link for editing user information.
      array(
        'data' => l(t('edit'), '#', array(
          'attributes' => array(
            'id' => 'editModal',
            'modal' => $rowses->taxid,
            'class' => 'asistant',
            'title' => t('Click to edit user information'),
          ),
        )
        ),
      ),
      // Link for deleting user information.
      array(
        'data' => l(t('delete'), '#', array(
          'attributes' => array(
            'id' => 'deleteModal',
            'modal' => $rowses->taxid,
            'class' => 'asist',
            'title' => t('Click to delete user information'),
          ),
        )
        ),
      ),
    );

  }
  $items['build']['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  $items['build']['pager'] = array(
    '#theme' => 'pager',
  );
  $items['change_form'] = array(
    '#type' => 'markup',
    '#markup' => drupal_render($not_taxable_users_change_item),
    '#prefix' => '<div id = "editmodal" class="wrapno"><h1>' . t('Edit user information') . '</h1>',
  );
  $items['delete_form'] = array(
    '#type' => 'markup',
    '#markup' => drupal_render($not_taxable_users_delete_item),
    '#prefix' => '</div><div id = "deletemodal" class="wrapnop"><h1>' . t('Delete user information?') . '</h1>',
    '#suffix' => '</div><div id="overlayer" style="display:none;"></div>',
  );
  return $items;
}

/**
 * Implements _form_validate().
 */
function not_taxable_users_search_validate($form, &$form_state) {
  $names = $form_state['values']['search_all'];
  $sqlw = db_query("SELECT uid FROM {users} WHERE name = :names", array(':names' => $names))->fetchField();
  // Checking the user's name.
  if (empty($sqlw)) {
    form_set_error('search_all', t("Write correct user name!"));
  }
  $form_state['values']['uid'] = $sqlw;
  // Checking if user alredy is applied for tax managing.
  $query = db_select('not_taxable_users', 'n');
  $query->join('users', 'u', 'n.uid = u.uid');
  $sqlp = $query
    ->fields('n', array('taxid', 'uid'))
    ->fields('u', array('uid', 'name'))
    ->condition('u.name', $names)
    ->execute()
    ->fetchField();
  if (!empty($sqlp)) {

    form_set_error('title', t("This user already exist in Managing table! Pleace edit him in the table"));
  }
}

/**
 * Implements _form_validate().
 */
function not_taxable_users_change_validate($form, &$form_state) {
  if (empty($form_state['values']['not_date'])) {
    form_set_error(t("Write correct date please!"));
  }
}

/**
 * Implements _form_submit().
 */
function not_taxable_users_search_submit($form, &$form_state) {
  $uid = $form_state['values']['uid'];
  $srchval = $form_state['values']['search_en'];
  // Converts string to timestamp.
  $srchdat = strtotime($form_state['values']['search_date']);
  if (empty($form_state['values']['search_date'])) {
    db_insert('not_taxable_users')
      ->fields(array('uid', 'not_taxable_users_val'))
      ->values(array(
        'uid' => $uid,
        'not_taxable_users_val' => $srchval,
      ))
      ->execute();
    drupal_set_message(t('User information has been saved'), 'status');
  }
  else {
    db_insert('not_taxable_users')
      ->fields(array('uid', 'not_taxable_users_val', 'exp_date'))
      ->values(array(
        'uid' => $uid,
        'not_taxable_users_val' => $srchval,
        'exp_date' => $srchdat,
      ))
      ->execute();
    drupal_set_message(t('User information has been saved'), 'status');
    cache_clear_all();
  }
}

/**
 * Implements _form_submit().
 */
function not_taxable_users_change_submit($form, &$form_state) {
  $punam = $form_state['values']['notes_all'];
  $aviabl = $form_state['values']['notes_en'];
  $udate = strtotime($form_state['values']['not_date']);
  db_update('not_taxable_users')
    ->fields(array('not_taxable_users_val' => $aviabl, 'exp_date' => $udate))
    ->condition('taxid', $punam)
    ->execute();
  drupal_set_message(t('User information has been saved'), 'status');
}

/**
 * Implements _form_submit().
 */
function not_taxable_users_delete_submit($form, &$form_state) {
  $delnam = $form_state['values']['notes_one'];
  db_delete('not_taxable_users')
    ->condition('taxid', $delnam)
    ->execute();
  drupal_set_message(t('User information has been deleted'), 'status');
  cache_clear_all();
}
