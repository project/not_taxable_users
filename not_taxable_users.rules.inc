<?php

/**
 * @file
 * Default rule configurations for Not Taxable users module.
 */

  /**
   * Implements hook_rules_condition_info().
   */
function not_taxable_users_rules_condition_info() {
  $conditions['not_taxable_users_user_function'] = array(
    'label' => t("User is taxable"),
    'group' => t('Taxable user'),
    'base' => 'not_taxable_users_user_function',
  );
  // Checks if Commerce module exist.
  if (module_exists('commerce')) {
    $conditions['not_taxable_users_user_function']['parameter'] = array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    );
  }
  // Checks if Ubercart module exists.
  elseif (module_exists('uc_product')) {
    $conditions['not_taxable_users_user_function']['parameter'] = array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Order'),
      ),
    );
  }
  return $conditions;
}

/**
 * Action function.
 */
function not_taxable_users_user_function() {
  global $user;
  // Checks if user a user is logged in.
  if (user_is_logged_in()) {
    $cuid = $user->uid;
  }
  else {
    $cuid = 0;
  }
  // Current date.
  $pu_date = time();
  $query = db_select('not_taxable_users', 'n');
  $query->condition('uid', $cuid);
  $query->fields('n', array('not_taxable_users_val', 'exp_date'));
  $puresulti = $query->execute();
  global $_not_taxable_users_result;
  $_not_taxable_users_result = TRUE;
  foreach ($puresulti as $record) {
    // If sales permit value is positive and expiration date has not expired
    // condition in tax rule will return FALSE
    // so tax rule will be not implemented.
    if (isset($record->not_taxable_users_val) && $record->not_taxable_users_val == "Yes" && $record->exp_date >= $pu_date) {
      $_not_taxable_users_result = FALSE;
    }
  }
  return $_not_taxable_users_result;
}
