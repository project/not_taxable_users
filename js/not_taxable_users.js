/**
 * @file
 * This is the main Java Script file for Not Taxable users module.
 */

  (function ($) {
    'use strict';
    $(document).ready(function () {
      var a = 0;
      var b = 0;
      $('.asist').removeAttr('href');
      // Shows modal block with form for deleting user information from table.
      $('.asist').click(function () {
        $('#deletemodal').show();
        $('#overlayer').show();
        a = $(this).attr('modal');
        $('input[name=notes_one]').val(a);
      });
      $('.asistant').removeAttr('href');
      // Shows modal block with form for editing user information from table.
      $('.asistant').click(function () {
        $('#editmodal').show();
        $('#overlayer').show();
        b = $(this).attr('modal');
        $('input[name=notes_all]').val(b);
      });
      // Hides modal block with form for deleting user information from table.
      $('#overlayer').click(function () {
        $('#deletemodal').hide();
        $('#editmodal').hide();
        $(this).hide();
      });
      // Hides modal block with form for editing user information from table.
      $('#deletemodal .exit').click(function () {
        $('#deletemodal').hide();
        $('#overlayer').hide();
      });
    });
  })(jQuery);
