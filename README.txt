CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 
INTRODUCTION
------------
Not taxable users module offers the possibility to exclude sales
taxes from users orders throughout the managing users list on module's
amdinistration page. In some cases there are no taxes needed in online stores
for example, if a customer has a sales permit, the order for him is not
taxable. It can be done using Not Taxable Users module. The process of
applying users for non taxable orders is simple: just need to fill out two
fields in the webform: if sales permit exist and the expiration date
of the sales permit. Also adding condition in tax rule is required.


 * For a full description of the module, visit the project page:


 * To submit bug reports and feature suggestions, or to track changes:
   
   
REQUIREMENTS
------------
This module requires the following modules:
 * Rules (https://www.drupal.org/project/rules)
 * Date Popup (included in date module) (https://www.drupal.org/project/date)
 
 
  INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration > People > Permissions:


   - Access not taxable users


     Users in roles with the "Access administration menu" permission
     will be able to access the module configuration page.
     
     
   
 * Customize the user settings in 
 Administration>Configuration>System>Not taxable users

 *Setting up users for non taxable orders:
    - Add condition in tax rule is required for correct work this module:
      - For Drupal Commerce module: 
        -- Go to admin/commerce/config/taxes page and press on "configure
        component" link.
        -- Add a condition - "User is available for tax" from group "Taxable
        Users".
        -- On the next step Data selector will be - "commerce-line-item:order".
        -- Save changes.
      - For Ubercart module: 
        -- Go to admin/store/settings/taxes page and press on "conditions" link
        in your sales tax rule.
        -- Add a condition - "User is available for tax" from group "Taxable
        Users".
        -- On the next step Data selector will be - "order".
        -- Save changes.
    - On the top of config page fill out "Add user form".
    Enter name of the user and information about the sales permit. 
    - After submitting the form user information will appear in
    "Managing table".
    In the table user can be edited or deleted.  
   
MAINTAINERS
-----------
Current maintainers:
 * Anatolii Kosovilka (Anatolii88) - https://www.drupal.org/user/3063339
